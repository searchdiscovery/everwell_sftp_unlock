import csv
import datetime
import paramiko
import os
import zipfile
import shutil
from time import gmtime, strftime
from zipfile import ZipFile
from pathlib import Path
import pandas as pd

logfile = os.getcwd() + '/' + 'LOG.txt'
log = open(logfile, 'w')

# Formatting Date and Creating Directory
recenttime = datetime.datetime.now() - datetime.timedelta(minutes=25)

currenttime = datetime.datetime.now()
yesterday = datetime.datetime.now() - datetime.timedelta(days=1)

yesterdaydate = '{:02d}{:02d}{:04d}'.format(yesterday.month, yesterday.day, yesterday.year)
curdate = '{:02d}{:02d}{:04d}'.format(currenttime.month, currenttime.day, currenttime.year)

destination = 'DomoUpload'
if not os.path.exists(destination):
    os.mkdir(destination)


# This function downloads the latest file from SFTP server, using Paramiko

def file_download():
    password = 'S3OYy765EW7d0bh'
    host = 'sftp-drop.domo.com'
    username = 'everwellrx'
    port = 22
    transport = paramiko.Transport((host, port))
    zippass = b'799crx'
    latestfile = ''
    latest = 0
    boolRename = 'false'
    outFile = ''
    fullFileDetails = []
    relevant_files = []
    times_array = []

    transport.connect(username=username, password=password)
    sftp = paramiko.SFTPClient.from_transport(transport)
    log.write('\nCurrent Date %s' % curdate)
    log.write('\nYesterday Date %s' % yesterdaydate)

    for fileattr in sftp.listdir_attr():
        if (fileattr.filename.startswith('1002637DOMO' + curdate)) or (fileattr.filename.startswith('1002637DOMO' + yesterdaydate)):
            if fileattr.filename.endswith('.zip'):
                times_array.insert(0, fileattr.st_mtime)
                if fileattr.st_mtime > latest:
                    fullFileDetails.append(fileattr)
                    log.write('\nDetermining latest file...')
					
	# Locates the most 2 recent files and takes the larger one. 
    max_file = max(times_array)
    print('max file is %s' % max_file)
    times_array.remove(max_file)
    second_max_file = max(times_array)
    print('second max file is %s' % second_max_file)
    for f in fullFileDetails:
        if f.st_mtime == max_file or f.st_mtime == second_max_file:
            print('File found which matches one of most 2 recent files. %s ' % f)
            relevant_files.insert(0, f)

    if relevant_files[0].st_size > relevant_files[1].st_size:
        latestfile = relevant_files[0].filename
    else:
        latestfile = relevant_files[1].filename

    print('final latest is %s' % latestfile)

    if latestfile is not '':
        try:
            localpath = os.getcwd() + '/' + latestfile
            locallatest = Path(localpath)
            if locallatest.is_file():
                log.write('\nFile exists locally, not downloading')
                boolRename = 'false'
            else:
                log.write('\nDownloading files ==> %s' % latestfile)
                sftp.get(latestfile, localpath)
                with ZipFile(latestfile) as zf:
                    zf.setpassword(zippass)
                    outFile = zf.extractall(pwd=zippass)
                log.write('\nExtracted')
                boolRename = 'true'
        except IOError as e:
            log.write('\n %s' % e)
    else:
        log.write('No files found for today or yesterday.')
    sftp.close()
    log.write('\nClosed SFTP')
    transport.close()
    log.write('\nFile Download and Unzip completed')
    if (boolRename != 'false'):
        export_file(outFile, latestfile)
        log.write('\nRenaming file now')


# This function is called from the file_download function
# It moves the files to required directory and to the specified output file.

def export_file(data, filename):
    print(filename)
    trimmedName = filename[:-4]
    print(trimmedName)
    print(data)
    currentfile = os.getcwd() + '/AutoRunReports/' + trimmedName + '.csv'
    temp_file = os.getcwd() + '/' + trimmedName + '.zip'
    csv_input = pd.read_csv(currentfile, encoding="ISO-8859-1")
    csv_input['UploadDateTime'] = datetime.datetime.now()
    csv_input.to_csv('domoDataGOOD.csv', index=False, encoding="utf-8")
    log.write('\nAppended datetime')
    os.remove(temp_file)
    log.write('Removed temporary file')


def main():
    log.write('\nStarting to run at %s' % datetime.datetime.now())
    try:
        file_download()
    except Exception as e:
        log.write('\n %s' % e)
    log.write('\nClosing log at %s' % datetime.datetime.now())
    log.close()


if __name__ == '__main__':
    main()
