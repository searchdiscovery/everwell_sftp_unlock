# everwell_sftp_unlock

This script was written and deployed for EverwellRx Pharmacy (a Domo client). 

Project Manager: Sarah Baker  
TC: Shira Shkarofsky  
Summer 2018  

## This script:
* Logs into an SFTP server (in this case it's a Domo-hosted SFTP).
* Locates files based on filename, edit date, and file size
* Identifies the desired file
* Downloads, extracts, and gives access to the file from the password-protected ZIP folder.
* Moves file to a directory with all of the historical files
* Appends current date column onto data (similar to a Batch Last Run column in Domo)
* Saves the data to a specific file, which gets picked up by Domo Workbench.

